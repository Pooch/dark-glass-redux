# Dark-Glass-Redux
Gnome shell theme based off of [Dark-Glass-Pro](https://www.opendesktop.org/p/1262116).

**Note:** this is my first gnome shell theme, open to any advice.

Please report any issues to this repository.

# Screenshots
![Screenshot 1](./screenshots/dark-glass-redux-0.jpg)
![Screenshot 2](./screenshots/dark-glass-redux-1.jpg)
![Screenshot 3](./screenshots/dark-glass-redux-2.jpg)
![Screenshot 4](./screenshots/dark-glass-redux-3.jpg)

# Installation
In the gnome tweaks tool select: `Appearance -> Shell -> Select Dark-Glass-Redux.tar.gz`

You should then be able to select `Dark-Glass-Redux` as a theme.

# Making Changes
Modify the `gnome-shell.less` file, then generate the corresponding css file using the commands below:
```
cd dark-glass-redux/
npm i
npm run build
```
You should now have an updated `gnome-shell.css` file in the `gnome-shell` directory.

